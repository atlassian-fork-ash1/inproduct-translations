package com.atlassian.translations.inproduct.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:02
 */
public class AoTranslationsManager extends AbstractAoTranslationsManager {
    private final LocaleManager localeManager;
    private final I18NBeanFactory i18NBeanFactory;

    public AoTranslationsManager(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, ActiveObjects ao) {
        super(ao);
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    @Override
    protected String getText(String key) {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get())).getText(key);
    }
}
