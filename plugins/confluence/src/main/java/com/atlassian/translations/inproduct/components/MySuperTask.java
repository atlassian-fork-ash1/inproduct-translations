package com.atlassian.translations.inproduct.components;

import com.atlassian.core.task.Task;

import java.io.Serializable;
import java.util.concurrent.Callable;

import static java.util.Objects.requireNonNull;

/**
 * Created by mfedoryshyn on 20/05/2016.
 */
public class MySuperTask implements Task {
    private final Callable<Serializable> taskToExecute;

    public MySuperTask(Callable<Serializable> taskToExecute) {
        this.taskToExecute = requireNonNull(taskToExecute);
    }

    @Override
    public void execute() throws Exception {
        taskToExecute.call();
    }
}
