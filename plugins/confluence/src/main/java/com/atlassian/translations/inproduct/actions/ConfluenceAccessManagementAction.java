package com.atlassian.translations.inproduct.actions;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.translations.inproduct.components.ConfluenceAccessManager;
import com.google.common.base.Objects;
import com.opensymphony.xwork.ActionContext;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: kalamon
 * Date: 25.06.13
 * Time: 15:22
 */
public class ConfluenceAccessManagementAction extends AbstractIptAction {
    private final ConfluenceAccessManager accessManager;

    protected ConfluenceAccessManagementAction(ConfluenceAccessManager accessManager, WebResourceManager webResourceManager) {
        super(webResourceManager);
        this.accessManager = accessManager;
    }

    public String getName() {
        return "access-save.action";
    }

    public List<ConfluenceAccessManager.GroupSpec> getGroups() {
		List<ConfluenceAccessManager.GroupSpec> groups = new CopyOnWriteArrayList<ConfluenceAccessManager.GroupSpec>(accessManager.getGroups());

		for (ConfluenceAccessManager.GroupSpec group: groups) {
			if (group.getGroupName().contains("\n") || group.getGroupName().contains("\r")) {
				groups.remove(group);
			}
		}

		return groups;
    }

    @Override
    public String execute() throws Exception {
        wrm.requireResource("com.atlassian.translations.jira.inproduct:admin-access-javascript");
        String res = super.execute();
        if (Objects.equal(SUCCESS, res)) {
            if (xsrfOk()) {
                final Map<String, String[]> requestParameters = ActionContext.getContext().getParameters();
                final String[] groups = requestParameters.get("groups");
                accessManager.setGroups(groups);
                return SUCCESS;
            } else {
                return ERROR;
            }
        }
        return res;
    }
}
