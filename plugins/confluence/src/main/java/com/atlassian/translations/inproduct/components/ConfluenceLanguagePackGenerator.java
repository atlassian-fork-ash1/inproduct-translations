package com.atlassian.translations.inproduct.components;

import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.Map;

/**
 * User: kalamon
 * Date: 12.04.13
 * Time: 12:06
 */
public class ConfluenceLanguagePackGenerator extends AbstractLanguagePackGenerator {
    public ConfluenceLanguagePackGenerator(TranslationManager translationManager) {
        super(translationManager);
    }

    @Override
    protected String getProductName() {
        return "Confluence";
    }

    @Override
    protected String getWebActionSupportPath() {
        return "com/atlassian/confluence/core/ConfluenceActionSupport";
    }

    @Override
    protected String renderLanguagePackXml(Map<String, Object> context) {
        return VelocityUtils.getRenderedTemplate("/templates/vm/admin/languagepack.vm", context);
    }
}
