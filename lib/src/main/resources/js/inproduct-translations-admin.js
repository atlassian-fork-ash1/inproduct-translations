//Extracted from translations.vm

getCurrentLocaleAnd = function (callback, onError) {
    var me = this;
    var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/locales?_dummy=' + new Date().getTime();
    jQuery.ajax({
        type:"GET",
        url:endpoint,
        success:function (data, status, xhr) {
            callback(data.currentLocale.name);
        },
        error:function (xhr, status, error) {
            onError(xhr);
        }
    });
};

uploadToTac = function(login, password, cb) {
    getCurrentLocaleAnd(
        function(locale) {
            var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/contributetotac?_dummy=' + new Date().getTime();
            var data = {locale: encodeURIComponent(locale), login: login, password: password};
            AJS.safeAjax({
                type:"POST",
                url:endpoint,
                data:data,
                success:function (data, status, xhr) {
                    cb({ success: true, result: data });
                },
                error:function (xhr, status, error) {
                    cb({success: false, error: xhr.status + ' ' + xhr.statusText});
                }
            });
        },
        function(xhr) {
            cb({success: false, error: xhr.status + ' ' + xhr.statusText});
        }
    );
};

uploadDialog = function () {
    AJS.$('#inproduct-tac-upload-dialog').remove();
    var popup = new AJS.Dialog({ width:600, height:350, id:'inproduct-tac-upload-dialog'});
    popup.addHeader(AJS.I18n.getText("inproduct.admin.tac.upload.question"));
    popup.addPanel("Panel 1", "panel1");
    var html = InProductTranslation.uploadToTacDialog();
    html = html.replace('&lt;', '<').replace('&gt;', '>');
    popup.getCurrentPanel().html(html);
    popup.addButton(AJS.I18n.getText("inproduct.admin.yes"), function (dialog) {
        var login = AJS.$('#inproduct-tac-upload-dialog').find('input[name="login"]').val();
        var pwd = AJS.$('#inproduct-tac-upload-dialog').find('input[name="password"]').val();
        popup.getCurrentPanel().html('<div class="inproduct-throbber"></div>');
        AJS.$('#inproduct-tac-upload-dialog').find('.button-panel-button').hide();
        AJS.$('#inproduct-tac-upload-dialog').find('.button-panel-link').html(AJS.I18n.getText("inproduct.admin.close")).hide();
        uploadToTac(login, pwd, function(res) {
            if (res.success && (typeof res.result.error === 'undefined')) {
                var number = res.result.uploadedCount;
                var txt = AJS.$('<div>')
                    .append(AJS.$('<div>').html(AJS.I18n.getText("inproduct.admin.upload.result.success")));
                if (number != null) {
                    var description = AJS.I18n.getText("inproduct.admin.upload.result.success.number");
                    description = description.replace("<NUMBER>", number);
                    txt.append(AJS.$('<div>').html(description));
                }

                if (res.result.hasOwnProperty("failedTranslations") && res.result.failedTranslations.length != 0) {
                    var table = AJS.$('<table class="aui">');
                    table.append(AJS.$('<tr>')
                        .append(AJS.$('<th>').html(AJS.I18n.getText("inproduct.admin.key")))
                        .append(AJS.$('<th>').html(AJS.I18n.getText("inproduct.admin.message")))
                        .append(AJS.$('<th>').html(AJS.I18n.getText("inproduct.admin.errorMessage"))));

                    var failedJson = res.result.failedTranslations
                    _.each(failedJson, function(element) {
                        var row = AJS.$('<tr>')
                            .append(AJS.$('<td>').html(element.i18nKey))
                            .append(AJS.$('<td>').html(element.translation))
                            .append(AJS.$('<td>').html(element.errorInfo));
                        table.append(row);
                    })

                    txt.append(AJS.$('<div>').html(AJS.I18n.getText("inproduct.admin.failed.header").replace('<NUMBER>', failedJson.length))
                        .append(' ')
                        .append(AJS.$('<a id="failed-replace-text-trigger" class="aui-expander-trigger" aria-controls="failed-replace-text-content">')
                            .attr('data-replace-text', AJS.I18n.getText("inproduct.admin.hide"))
                            .html(AJS.I18n.getText("inproduct.admin.show")))
                        .append(AJS.$('<div id="failed-replace-text-content" class="aui-expander-content">')
                            .append(table)));
                }

                if (res.result.hasOwnProperty("notSavedTranslations") && res.result.notSavedTranslations.length != 0) {
                    var table = AJS.$('<table class="aui">');
                    table.append(AJS.$('<tr>')
                        .append(AJS.$('<th>').html(AJS.I18n.getText("inproduct.admin.key")))
                        .append(AJS.$('<th>').html(AJS.I18n.getText("inproduct.admin.message"))));

                    var notSavedJson = res.result.notSavedTranslations;
                    _.each(notSavedJson, function(element) {
                        var row = AJS.$('<tr>')
                            .append(AJS.$('<td>').html(element.i18nKey))
                            .append(AJS.$('<td>').html(element.translation));
                        table.append(row);
                    });

                    var product;
                    if(AJS.$('meta[name="application-name"]')[0]) {
                        product = AJS.$('meta[name="application-name"]')[0].content;
                    } else {
                        // Confluence page has no meta tag that gives the application name
                        product = 'Confluence';
                    }

                    txt.append(AJS.$('<div>').html(AJS.I18n.getText("inproduct.admin.notSaved.header").replace('<NUMBER>', notSavedJson.length).replace('<PRODUCT>', product))
                        .append(' ')
                        .append(AJS.$('<a id="not-saved-replace-text-trigger" class="aui-expander-trigger" aria-controls="not-saved-replace-text-content">')
                            .attr('data-replace-text', AJS.I18n.getText("inproduct.admin.hide"))
                            .html(AJS.I18n.getText("inproduct.admin.show")))
                        .append(AJS.$('<div id="not-saved-replace-text-content" class="aui-expander-content">')
                            .append(table)));
                }

                popup.getCurrentPanel().html(txt);
            } else if (res.success && (typeof res.result.error !== 'undefined')) {
                var txt = AJS.I18n.getText("inproduct.admin.upload.result.error");
                txt = txt.replace('<ERROR>', res.result.error);
                popup.getCurrentPanel().html(txt);
            } else {
                var txt = AJS.I18n.getText("inproduct.admin.upload.result.error");
                txt = txt.replace('<ERROR>', res.error);
                popup.getCurrentPanel().html(txt);
            }
            AJS.$('#inproduct-tac-upload-dialog').find('.button-panel-link').show();
        });
    });

    popup.addLink(AJS.I18n.getText("inproduct.admin.no"), function (dialog) {
        popup.hide();
        popup.remove();
    });

    popup.show();
    AJS.$('#inproduct-tac-upload-dialog').find('input[name="login"]').focus();
    AJS.$('#inproduct-tac-upload-dialog').find('button').addClass('aui-button');
    var txt = AJS.I18n.getText("inproduct.admin.upload.to.tac.description");
    if(AJS.$('meta[name="application-name"]')[0]) {
        txt = txt.replace('<PRODUCT>', AJS.$('meta[name="application-name"]')[0].content);
    } else {
        // Confluence page has no meta tag that gives the application name
        txt = txt.replace('<PRODUCT>', 'Confluence');
    }
    AJS.$('#inproduct-tac-upload-dialog').find('.description').html(txt);
};

AJS.$(document).ready(function() {
    AJS.$('.deletekey').click(function(e) {
        e.preventDefault();
        var id = e.currentTarget.id.split('_')[1];
        AJS.$('#keytodelete').val(id);
        AJS.$('#keytodelete').parents('form').submit();
    });
    AJS.$('#inproduct_upload_to_tac').click(function(e) {
        e.preventDefault();
        uploadDialog();
    })
});