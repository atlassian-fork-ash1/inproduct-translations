package com.atlassian.translations.inproduct.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.translations.inproduct.ao.entity.AllowedGroupEntity;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AoAllowedGroupsManager {

	private final ActiveObjects ao;
	private static final Logger LOG = Logger.getLogger(AoAllowedGroupsManager.class);

	public AoAllowedGroupsManager(ActiveObjects ao) {
		this.ao = ao;
	}

	public void setAllowedGroups(final Collection<String> groups) {
		ao.executeInTransaction(new TransactionCallback<AllowedGroupEntity>() {
			@Override
			public AllowedGroupEntity doInTransaction() {
                deleteAllGroups();
                if (groups == null) {
                    return null;
                }
                for (String group : groups) {
                    final AllowedGroupEntity entity = ao.create(AllowedGroupEntity.class);
                    entity.setName(group);
                    entity.save();
                }
                return null;
			}
		});
	}

	public Collection<String> getAllowedGroups() {
        AllowedGroupEntity[] entities = ao.find(AllowedGroupEntity.class);

        if (entities == null || entities.length == 0) {
            return null;
        }

        List<String> result = new ArrayList<String>();
        for (AllowedGroupEntity entity : entities) {
            result.add(entity.getName());
        }

		return result;
	}

	private void deleteAllGroups() {
        AllowedGroupEntity[] entities = ao.find(AllowedGroupEntity.class);

		for (AllowedGroupEntity entity : entities) {
			ao.delete(entity);
		}
	}
}
