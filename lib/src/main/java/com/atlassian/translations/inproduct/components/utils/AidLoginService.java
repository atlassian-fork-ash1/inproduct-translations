package com.atlassian.translations.inproduct.components.utils;

import com.google.gson.Gson;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AidLoginService {
    private static final HttpHost loginHost = new HttpHost("id.atlassian.com", 443, "https");
    private static final String loginPath = "/id/rest/login";
    private static final String LOGIN_TOKEN_NAME = "__ATL_TOKEN";

    public static final Logger log = LoggerFactory.getLogger(AidLoginService.class);

    private static class Login {
        private Login(String username, String password) {
            this.username = username;
            this.password = password;
        }

        private String username;
        private String password;
    }

    public String login(String login, String password) throws IOException {
        Login l = new Login(login, password);
        String gson = new Gson().toJson(l);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost req = new HttpPost(loginPath);
        StringEntity entity = new StringEntity(gson, ContentType.APPLICATION_JSON);
        req.addHeader("Content-Type", "application/json");
        req.setEntity(entity);

        //Use the following only for test purpouses (integration with stg/dev IAC)
        //client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
        client.execute(loginHost, req);
        CookieStore cookieStore = client.getCookieStore();
        List<Cookie> cookies = cookieStore.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(LOGIN_TOKEN_NAME)) {
                return cookie.getValue();
            }
        }
        log.warn("Login failed. " + LOGIN_TOKEN_NAME + " cookie not found in response to login.");
        throw new IOException("Login failed");
    }
}
