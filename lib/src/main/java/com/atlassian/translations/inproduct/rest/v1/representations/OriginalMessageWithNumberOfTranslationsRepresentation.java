package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class OriginalMessageWithNumberOfTranslationsRepresentation {
	@XmlElement
	private String message;
	@XmlElement
	private boolean isAnyTranslation;

	public OriginalMessageWithNumberOfTranslationsRepresentation(String message, boolean isAnyTranslation) {
		this.message = message;
		this.isAnyTranslation = isAnyTranslation;
	}
}
