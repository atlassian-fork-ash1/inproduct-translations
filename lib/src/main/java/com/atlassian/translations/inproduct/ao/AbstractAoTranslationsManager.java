package com.atlassian.translations.inproduct.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.translations.inproduct.MessageTransformUtil;
import com.atlassian.translations.inproduct.ao.entity.TranslationsEntity;
import com.atlassian.translations.inproduct.components.TranslationManager;
import net.java.ao.Query;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
public abstract class AbstractAoTranslationsManager implements TranslationManager {

	private final ActiveObjects ao;
	private static final Logger LOG = Logger.getLogger(AbstractAoTranslationsManager.class);

	protected AbstractAoTranslationsManager(ActiveObjects ao) {
		this.ao = ao;
	}

	public void saveTranslation(final String locale, final String key, final String translation) {

		ao.executeInTransaction(new TransactionCallback<TranslationsEntity>() {
			@Override
			public TranslationsEntity doInTransaction() {

				Query query = Query.select().where("LOCALE = ? AND KEY = ?", locale, key);
				TranslationsEntity[] entities = ao.find(TranslationsEntity.class, query);
				
				if (entities.length > 1) {
					LOG.warn("Multiple messages per key detected but it is not supported yet.");
				}

				// delete all previous translations for a given key and locale
				for (TranslationsEntity entity : entities) {
					ao.delete(entity);
				}

				// save new translation for a given key and locale
				final TranslationsEntity translationsEntity = ao.create(TranslationsEntity.class);
				translationsEntity.setTranslation(translation);
				translationsEntity.setKey(key);
				translationsEntity.setLocale(locale);
				translationsEntity.save();
				return translationsEntity;
			}
		});

	}

	public String getTranslation(final String locale, final String key) {
		Query query = Query.select().where("LOCALE = ? AND KEY = ?", locale, key);

		TranslationsEntity[] entities = null;

        try {
            entities = ao.find(TranslationsEntity.class, query);
        } catch (IllegalStateException e) {
            LOG.debug("ActiveObjects not ready: " + e.getMessage());
        }

        if (entities == null) {
            return null;
        }

		if (entities.length > 1) {
			LOG.warn("Multiple messages per key detected but it is not supported yet.");
		}

		if (entities.length > 0) {
			return entities[0].getTranslation();
		}

		return null;
	}

    public TranslationsEntity[] getAllTranslations() {
        return ao.find(TranslationsEntity.class);
    }

    @Override
    public Map<String, String> getAllTranslations(String locale) {
        Map<String, String> result = new HashMap<String, String>();
        for (TranslationsEntity entity : getAllTranslations()) {
            result.put(entity.getKey(), entity.getTranslation());
        }
        return result;
    }

    /**
	 *
	 * @param locale
	 * @param key
	 * @return returns message for deleted local translation (not decorated)
	 */
	public String deleteTranslation(final String locale, final String key) {

		Query query = Query.select().where("LOCALE = ? AND KEY = ?", locale, key);
		TranslationsEntity[] entities = ao.find(TranslationsEntity.class, query);

		if (entities.length > 1) {
			LOG.warn("Multiple messages per key detected but it is not supported yet.");
		}
		
		// delete all translations for a given key and locale
		for (TranslationsEntity entity : entities) {
			ao.delete(entity);
		}

		return MessageTransformUtil.stripDecoration(getText(key));
	}

	@Override
	public void deleteAllTranslations(String locale) {
		Query query = Query.select().where("LOCALE = ?", locale);
		TranslationsEntity[] entities = ao.find(TranslationsEntity.class, query);

		// delete all translations for a given key and locale
		for (TranslationsEntity entity : entities) {
			ao.delete(entity);
		}
	}

	@Override
	public boolean isAnyTranslation(String locale) {
		Query query = Query.select().where("LOCALE = ?", locale);
		return ao.count(TranslationsEntity.class, query) > 0;
	}

    protected abstract String getText(String key);
}
