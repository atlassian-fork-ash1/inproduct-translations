package com.atlassian.translations.inproduct.components;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * User: kalamon
 * Date: 06.07.12
 * Time: 11:59
 */
public class MessageKeyGuesser {
    private static final long INTERVAL = 60000; // 1 minute

    private static final Logger LOG = Logger.getLogger(MessageKeyGuesser.class);
    public static final String NULL_KEY_FRAGMENT = "***nullKeyFragment***";

    private static class GuessEntry {
        private long timestamp;
        private String message;

        private GuessEntry(String message) {
            this.message = message;
            timestamp = new Date().getTime();
        }
    }

    public static class KeyAndMessageQueryAndResult {
        private List<String> keyMatches = new ArrayList<String>();
        private String message;
        private String keyFragment;
        private int index;

        public KeyAndMessageQueryAndResult(String keyFragment, String message, int index) {
            this.keyFragment = keyFragment;
            this.message = message;
            this.index = index;
        }

        public List<String> getKeyMatches() {
            return keyMatches;
        }

        public void addKey(String key) {
            keyMatches.add(key);
        }

        public String getMessage() {
            return message;
        }

        public String getKeyFragment() {
            return keyFragment;
        }

        public int getIndex() {
            return index;
        }
    }

    private Map<String, Map<String, GuessEntry>> guessEntries = new HashMap<String, Map<String, GuessEntry>>();

    public synchronized void add(Locale locale, String key, String message) {
        Map<String, GuessEntry> map = guessEntries.get(locale.toString());
        if (map == null) {
            map = new HashMap<String, GuessEntry>();
            guessEntries.put(locale.toString(), map);
        }
        map.put(key, new GuessEntry(message));
    }

    public synchronized void find(Locale locale, List<KeyAndMessageQueryAndResult> query) {
        Map<String, GuessEntry> map = guessEntries.get(locale.toString());
        if (map == null) {
            return;
        }
        long now = new Date().getTime();
        List<String> toRemove = new ArrayList<String>();
        for (String key : map.keySet()) {
            long age = now - map.get(key).timestamp;
            if (age > INTERVAL) {
//                LOG.warn("age " + age + ", removing" + key);
                toRemove.add(key);
            } else {
                match(key, map.get(key).message, query);
            }
        }
        for (String key : toRemove) {
            map.remove(key);
        }
    }

    private void match(String key, String message, List<KeyAndMessageQueryAndResult> query) {
        if ("common.words.assign".equals(key)) {
            int i = 0;
        }
        for (KeyAndMessageQueryAndResult item : query) {
            String keyFragment = item.getKeyFragment();
            if (item.getMessage().equals(message) && key.startsWith(keyFragment)) {
//                LOG.warn("adding key to result: " + key);
                item.addKey(key);
            } else if (message.startsWith(item.getMessage()) && (keyFragment.startsWith(NULL_KEY_FRAGMENT) || StringUtils.isEmpty(keyFragment))) {
                item.addKey(key);
            }
        }
    }
}
