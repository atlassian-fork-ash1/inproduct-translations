package com.atlassian.translations.inproduct.components;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: kalamon
 * Date: 10.07.12
 * Time: 15:20
 */
public class MessageCache {
	// locale is a key
    private Map<String, Map<String, String>> localeMessages = new ConcurrentHashMap<String, Map<String, String>>();

    public void save(String locale, String key, String translation) {
		Map<String, String> messages = getMessages(locale);

		messages.put(key, translation);
    }

	public String get(String locale, String key) {
		Map<String, String> messages = getMessages(locale);

        return messages.get(key);
    }

    public void delete(String locale, String key) {
		Map<String, String> messages = getMessages(locale);

		messages.remove(key);
    }

	public void delete(String locale) {
		Map<String, String> messages = getMessages(locale);

		messages.clear();
	}

	/**
	 *
	 * @param locale
	 * @return map with message key as a map key
	 */
	public Map<String, String> getMessages(String locale) {
		Map<String, String> messages = localeMessages.get(locale);
		if (messages == null) {
			messages = new ConcurrentHashMap<String, String>();
			localeMessages.put(locale, messages);
		}
		return messages;
	}
}
